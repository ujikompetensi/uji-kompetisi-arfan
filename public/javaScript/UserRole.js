const firebaseConfig = {
    apiKey: "AIzaSyCAcMqZZeNMfWVjLLeYSxvjr5PFY8ZTL5A",
    authDomain: "school-duty.firebaseapp.com",
    databaseURL: "https://school-duty-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "school-duty",
    storageBucket: "school-duty.appspot.com",
    messagingSenderId: "525761571882",
    appId: "1:525761571882:web:5897b99cba356b42bfa753"
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const database = firebase.database();

function userRole() {
    firebase.auth().onAuthStateChanged(function (user) {
        database.ref('users/' + user.uid + '/UserData').once('value')
            .then(function (snaphot) {
                var data = snaphot.val().role;
                var x = document.getElementById('homework');

                if (data != 'Guru') {
                    x.setAttribute('onClick', "window.location.replace('Guru.html')")
                } else {
                    x.setAttribute('onClick', "window.location.replace('Homework.html')")
                }
            })

        var storageRef = firebase.storage().ref('userProfile');
        storageRef.child(user.uid).getDownloadURL().then(function (url) {
            document.getElementById('img').src = url;
         }).catch(function (error) {});
    });
}

userRole()