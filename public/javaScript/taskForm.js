const firebaseConfig = {
    apiKey: "AIzaSyCAcMqZZeNMfWVjLLeYSxvjr5PFY8ZTL5A",
    authDomain: "school-duty.firebaseapp.com",
    databaseURL: "https://school-duty-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "school-duty",
    storageBucket: "school-duty.appspot.com",
    messagingSenderId: "525761571882",
    appId: "1:525761571882:web:5897b99cba356b42bfa753"
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const database = firebase.database();

window.addEventListener("load", function () {
    const loader = document.querySelector(".loader");
    loader.className += " hidden"; // class "loader hidden"
});

function addTask() {
    var task = document.getElementById('task');

    var Addtasktittle = document.getElementById('addTaskTitle').value;
    var Addtaskanswer = document.getElementById('addTaskAnswer').value;
    var Addtask = document.getElementById('addTask').value;
    // cari tau ini dari mana
    var Material = document.getElementById('material').value;
    var Date = document.getElementById('date').value;

    var user = auth.currentUser;
    var database_ref = database.ref("Task");

    if (document.querySelector("input").value.length == 0) {
        alert("empty")
    } else {
        firebase.auth().onAuthStateChanged(function (user) {
            database_ref.child(user.uid).child(Material).orderByChild("taskTittle").equalTo(Addtasktittle).once("value", snapshot => {
                if (snapshot.exists()) {
                    alert('A task with that title already exist')
                } else {
                    database_ref.child(user.uid).child(Material).child(Addtasktittle).set({
                        taskTittle: Addtasktittle,
                        task: Addtask,
                        taskAnswer: Addtaskanswer,
                        tastTypeMath: Material,
                        taskExpiraton: Date
                    }).then(
                        window.location.replace('Homework.html')
                    );
                }
            })
        });
    }
}

function addItemTolist(_taskType, _tasktittle, _taskExpiration) {
    var task = document.getElementById('container');
    var Container = document.createElement('div');
    var TaskType = document.createElement('p');
    var TaskTittle = document.createElement('h2');
    var TaskExpiration = document.createElement('p');

    // class="card p-3 bg-light mw-5% mh-75%"
    Container.setAttribute('id', _tasktittle);
    Container.setAttribute('Class', 'card p-3 bg-light w-75');
    Container.setAttribute('onClick', 'SelectData()');

    TaskTittle.setAttribute('Class', 'mb-4');
    TaskTittle.setAttribute('id', 'TaskTitle');
    TaskTittle.innerHTML = _tasktittle;

    TaskType.setAttribute('id', 'TaskId');
    TaskType.innerHTML = "";

    TaskExpiration.setAttribute('id', 'TaskExpiration')
    TaskExpiration.innerHTML = "Until " + _taskExpiration;

    task.appendChild(Container);

    Container.appendChild(TaskType);
    Container.appendChild(TaskTittle);
    Container.appendChild(TaskExpiration);
}

function FetchAllDatabase() {
    firebase.auth().onAuthStateChanged(function (user) {
        database.ref('Task/' + user.uid).child('Math').orderByChild('tasks' + 'taskTittle').once('value')
            .then(function (snaphot) {
                snaphot.forEach(function (ChildSnapshot) {
                    let _taskType = ChildSnapshot.val().material;
                    let _tasktittle = ChildSnapshot.val().taskTittle;
                    let _taskExpiration = ChildSnapshot.val().taskExpiraton;

                    addItemTolist(_taskType, _tasktittle, _taskExpiration);
                });
            })
    });
}

window.onload(FetchAllDatabase());

function SelectData() {
    var div = document.getElementById('container');

    div.addEventListener('click', function (e) {
        if (e.target) {
            window.location.href = "../Codes/TaskDetail.html"
            localStorage.setItem("first", e.target.id);
        }
    }, {
        once: true
    })
}

function userRole() {
    firebase.auth().onAuthStateChanged(function (user) {
      database.ref('/users/' + user.uid).once('value')
        .then(function (snaphot) {
          var data = snaphot.child('UserData').val().role;
          var x = document.getElementById('hmAddBtn');
  
          if (data != 'Guru') {
            x.remove()
          } else {
            x.style.visibility = "visible"
          }
        })
    });
  }