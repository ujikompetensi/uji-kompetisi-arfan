const firebaseConfig = {
    apiKey: "AIzaSyCAcMqZZeNMfWVjLLeYSxvjr5PFY8ZTL5A",
    authDomain: "school-duty.firebaseapp.com",
    databaseURL: "https://school-duty-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "school-duty",
    storageBucket: "school-duty.appspot.com",
    messagingSenderId: "525761571882",
    appId: "1:525761571882:web:5897b99cba356b42bfa753"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig)

const auth = firebase.auth();
const database = firebase.database();

window.addEventListener("load", function () {
    const loader = document.querySelector(".loader");
    loader.className += " hidden"; // class "loader hidden"
});

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        console.log("there is no user")
    } else {
        console.log("there is user")
    }
});

function ResetPassword() {
    var email = document.getElementById("resetPassword").value;

    auth.sendPasswordResetEmail(email)
        .then(() => {
            alert('Password Reset Email Sent Successfully');
        })
        .catch(error => {
            console.error(error);
        })
}

//signup function
function signUp() {
    var username = document.getElementById('usernameReg').value;
    var email = document.getElementById('emailReg').value;
    var password = document.getElementById('passwordReg').value;
    var roleButton = document.getElementById('roleButton');

    var x = document.getElementById('snackbar');

    if (document.querySelector('input').value.length == 0 || roleButton.innerHTML.trim() == "") {
        alert('empty')
    } else {
        const promise = auth.createUserWithEmailAndPassword(email, password)
            .then(() => {
                var user = auth.currentUser

                if (roleButton.innerHTML == 'Guru') {
                    database.ref('users/').child(user.uid).set({
                      idGuru: user.uid
                    })
                }
                database.ref('users/' + user.uid + '/UserData').set({
                    email: email,
                    password: password,
                    username: username,
                    role: roleButton.innerHTML,
                    last_login: Date.now(),
                    profilePicture: '../dummy.svg'
                }).then(() => {
                    x.className = "show";
                    x.innerHTML = "User registered"

                    setTimeout(function () {
                        x.className = x.className.replace("show", "hidden");
                    }, 1000)

                    setTimeout(function () {
                        window.location.replace('main.html');
                    }, 1000)
                });
            })
            .catch(function (error) {
                x.className = "show";
                x.innerHTML = error.message

                setTimeout(function () {
                    x.className = x.className.replace("show", "hidden");
                }, 1000)
            });
    }
}

function dropDownList() {
    var li = document.getElementById('ul');
    var roleButton = document.getElementById('roleButton');

    li.addEventListener('click', function (e) {
        if (e.target) {
          roleButton.innerHTML = e.target.innerHTML;
        }
    }, {
        once: true
    });
}

//signIN function
function signIn() {
    var email = document.getElementById("emailLog").value;
    var password = document.getElementById("passwordLog").value;
    var x = document.getElementById("snackbar");

    if (document.querySelector("input").value.length == 0) {
        alert("empty")
    } else {
        auth.signInWithEmailAndPassword(email, password)
        .then(() => {

            var user = auth.currentUser

            var database_ref = database.ref()

            var user_data = {
                last_login: Date.now()
            }

            database_ref.child('users/' + user.uid + '/UserData').update(user_data)

            x.className = "show";
            x.innerHTML = "User logged in";
            setTimeout(function () {
                x.className = x.className.replace('show', 'hidden');
            }, 1000)
            setTimeout(function () {
                window.location.replace('Main.html');
            }, 1000)

        })
        .catch(function (error) {
            x.className = "show";
            x.innerHTML = error.message
            setTimeout(function () {
                x.className = x.className.replace("show", "hidden");
            }, 1000)
        })
    }
    
}