function userHmRole() {
    firebase.auth().onAuthStateChanged(function (user) {
        database.ref('/users/' + user.uid).once('value')
            .then(function (snaphot) {
                var data = snaphot.child('UserData').val().role;
                var x = document.getElementById('openForm');
                var y = document.getElementById('homework');

                if (data != 'Guru') {
                    x.remove()
                    y.setAttribute('onClick', 'window.location.replace("Guru.html")')
                } else {
                    x.style.visibility = 'visible'
                }
            })
    });
}

function addTask() {
    var task = document.getElementById('task');

    var Addtasktittle = document.getElementById('addTaskTitle').value;
    var Addtaskanswer = document.getElementById('addTaskAnswer').value;
    var Addtask = document.getElementById('addTask').value;
    // cari tau ini dari mana
    var Material = document.getElementById('material').value;
    var Date = document.getElementById('date').value;

    var user = auth.currentUser;
    var database_ref = database.ref("Task");

    if (document.querySelector("input").value.length == 0) {
        alert("empty")
    } else {
        firebase.auth().onAuthStateChanged(function (user) {
            database_ref.child(user.uid + '/tugasSiswa').orderByChild("taskTittle").equalTo(Addtasktittle).once("value", snapshot => {
                if (snapshot.exists()) {
                    alert('A task with that title already exist')
                } else {
                    database_ref.child(user.uid + '/tugasSiswa/' + Addtasktittle).set({
                        taskTittle: Addtasktittle,
                        task: Addtask,
                        taskAnswer: Addtaskanswer,
                        tastType: Material,
                        taskExpiraton: Date
                    }).then(
                        window.location.replace('Homework.html')
                    );
                }
            })
        });
    }
}

function addItemTolist(_taskType, _tasktittle, _taskExpiration, id, role) {
    var task = document.getElementById('container');
    var Container = document.createElement('div');
    var TaskType = document.createElement('p');
    var TaskTittle = document.createElement('h2');
    var TaskExpiration = document.createElement('p');
    var deleteButton = document.createElement('button')

    // class="card p-3 bg-light mw-5% mh-75%"
    Container.setAttribute('id', _tasktittle);
    Container.setAttribute('Class', 'card p-2 bg-light me-2');
    Container.setAttribute('onClick', 'SelectData()');
    Container.setAttribute('styles','max-height','10rem')

    TaskTittle.setAttribute('Class', 'text-center');
    TaskTittle.setAttribute('id', 'TaskTitle');
    TaskTittle.innerHTML = _tasktittle;

    TaskType.setAttribute('id', 'TaskId');
    TaskType.innerHTML = "";

    TaskExpiration.setAttribute('id', 'TaskExpiration')
    TaskExpiration.setAttribute('class', 'mt-4')
    TaskExpiration.innerHTML = "Until " + _taskExpiration;

    if (role != 'Guru') {
        database.ref('Task/' + id + '/hasilSiswa/' + _tasktittle).orderByChild('taskTitle').once('value')
            .then(function (snaphot) {
                snaphot.forEach(function (ChildSnapshot) {
                    let taskTitle = ChildSnapshot.val().taskTitle;
                    var title = document.getElementById(taskTitle);
                    title.remove();
                });
            })
    }

    task.appendChild(Container)
    Container.appendChild(TaskType)
    Container.appendChild(TaskTittle)
    Container.appendChild(TaskExpiration)
}

function FetchAllDatabase() {
    firebase.auth().onAuthStateChanged(function (user) {

        database.ref('/users/' + user.uid).once('value')
            .then(function (snaphot) {
                var role = snaphot.child('UserData').val().role;
                var teacherId = localStorage.getItem('idGuru');
                var id

                if (role != 'Guru') {
                    id = teacherId
                } else {
                    id = user.uid
                }

                database.ref('Task/' + id + '/tugasSiswa').orderByChild('tasks' + 'taskTittle').once('value')
                    .then(function (snaphot) {
                        snaphot.forEach(function (ChildSnapshot) {
                            let _taskType = ChildSnapshot.val().material;
                            let _tasktittle = ChildSnapshot.val().taskTittle;
                            let _taskExpiration = ChildSnapshot.val().taskExpiraton;

                            addItemTolist(_taskType, _tasktittle, _taskExpiration, id, role);
                        });
                    })
            })
    });
}

window.onload(FetchAllDatabase());

function SelectData() {
    var div = document.getElementById('container');

    div.addEventListener('click', function (e) {
        if (e.target) {
            window.location.href = "../Codes/TaskDetail.html"
            localStorage.setItem('taskTitle', e.target.id);
        }
    }, {
        once: true
    })
}

function openTaskForm() {
    var x = document.getElementById('taskForm');
    x.className = "show"
}

function closeTaskForm() {
    var x = document.getElementById('taskForm');
    x.className = "hidden"
    setTimeout(function () {
        x.className = ""
    }, 250)
}