taskDescription()

function userHmRole() {
  firebase.auth().onAuthStateChanged(function (user) {
    database.ref('/users/' + user.uid + '/UserData').once('value')
      .then(function (snaphot) {
        var data = snaphot.val().role;

        var resultContainer = document.getElementById('test');
        var p = document.getElementById('p');

        var answerInput = document.getElementById('answer');
        var buttonInput = document.getElementById('answerButton');
        var dltTask = document.getElementById('deleteTaskbtn');
        var jawaban = document.getElementById('taskAnswer');


        if (data != 'Guru') {
          // siswa
          dltTask.remove()
          jawaban.remove()
          resultContainer.remove()
          p.remove()

          answerInput.style.visibility = 'visible'
          buttonInput.style.visibility = 'visible'
        } else {
          answerInput.remove()
          buttonInput.remove()

          dltTask.style.visibility = 'visible'
          jawaban.style.visibility = 'visible'
          resultContainer.style.visibility = 'visible'
          p.style.visibility = 'visible'

          // Guru
        }
      })
  });
}

function taskDescription() {
  var teacherId = localStorage.getItem('idGuru');
  var tasktitle = localStorage.getItem('taskTitle');

    database.ref('Task/' + teacherId + '/tugasSiswa/' + tasktitle).once('value')
    .then(function (snapshot) {

      var task = snapshot.val().task;
      document.getElementById('taskDescription').innerHTML = task;

      var taskAnswer = snapshot.val().taskAnswer;
      document.getElementById('taskAnswer').innerHTML = "Jawaban : " + taskAnswer;
    })
}

function submitAnswer() {
  var answerInput = document.getElementById('answer').value;

  var teacherId = localStorage.getItem('idGuru');
  var tasktitle = localStorage.getItem('taskTitle');

  database.ref('Task/' + teacherId + '/tugasSiswa/' + tasktitle).once('value')
    .then(function (snapshot) {

      var task = snapshot.val().task;
      var taskAnswer = snapshot.val().taskAnswer;
      var result

      if (answerInput == taskAnswer) {
        result = 'correct!'
      } else {
        result = 'incorrect!'
      }

      firebase.auth().onAuthStateChanged(function (user) {
        database.ref('users/' + user.uid + '/UserData').once('value')
          .then(function (snaphot) {
            var studentName = snaphot.val().username;
            database.ref('Task/' + teacherId + '/hasilSiswa/' + tasktitle).child(user.uid).set({
              taskTitle: tasktitle,
              task: task,
              studentAnswer: answerInput,
              studentName: studentName,
              result: result
            }).then(
              window.location.replace('Homework.html')
            )
          })
      });
    })
}

function addItemTolist(_studentName, _studentAnswer, _studentResult) {
  var task = document.getElementById('container');
  var Container = document.createElement('div');
  var StudentName = document.createElement('p');
  var p = document.createElement('p');
  var StudentAnswer = document.createElement('h2');
  var StudentResult = document.createElement('p');

  // class="card p-3 bg-light mw-5% mh-75%"
  Container.setAttribute('id', _studentName);
  Container.setAttribute('Class', 'card p-2 bg-light me-2');
  Container.setAttribute('onClick', 'SelectData()');

  StudentAnswer.setAttribute('Class', 'text-center');
  StudentAnswer.setAttribute('id', 'TaskTitle');
  StudentAnswer.innerHTML = _studentAnswer;

  StudentName.setAttribute('id', 'TaskId');
  StudentName .innerHTML = "Nama : " + _studentName;

  p.innerHTML = " Jawaban siswa : "

  StudentResult.setAttribute('id', 'TaskExpiration')
  StudentResult.setAttribute('class', 'mt-4')
  StudentResult.innerHTML = "Hasil : " + _studentResult;

  task.appendChild(Container);

  Container.appendChild(StudentName);
  Container.appendChild(p);
  Container.appendChild(StudentAnswer);
  Container.appendChild(StudentResult);
}

function FetchAllDatabase() {
  var taskTitle = localStorage.getItem('taskTitle');

  firebase.auth().onAuthStateChanged(function (user) {
    database.ref('Task/' + user.uid + '/hasilSiswa/' + taskTitle).orderByChild('studentId').once('value')
      .then(function (snaphot) {
        snaphot.forEach(function (ChildSnapshot) {
          let _studentName = ChildSnapshot.val().studentName;
          let _studentAnswer = ChildSnapshot.val().studentAnswer;
          let _studentResult = ChildSnapshot.val().result;

          addItemTolist(_studentName, _studentAnswer, _studentResult);
        });
      })
  });
}

FetchAllDatabase()

// function answer() {

//     var answer = document.getElementById('answer').value;
//     var teacherId = localStorage.getItem("teacherId");
//     var tasktitle = localStorage.getItem("task");

//     database.ref('Task/' + teacherId).child('Biology').child(tasktitle).once('value')
//         .then(function (snapshot) {

//             var data = snapshot.val().tasks;
//             if (answer == data) {
//                 alert('correct!')
//             } else {
//                 alert('incorrect!')
//             }
//         })
// }