function addTeacher() {
    var teacherId = document.getElementById('insertId').value;

    if (document.querySelector("input").value.length == 0) {
        alert("empty")
    } else {
        firebase.auth().onAuthStateChanged(function (user) {
            database.ref('users').orderByChild('idGuru').equalTo(teacherId).once('value', snapshot => {
                if (snapshot.exists()) {
                    database.ref('users/' + user.uid + '/Guru').child(teacherId).set({
                        idGuru : teacherId
                    })
                    window.location.reload()
                } else {
                    alert('There no teacher with that id')
                }
            })
        });
    }
}

function addItemTolist(_namaGuru, _idGuru, _emailGuru, _pfpGuru) {

    // The Containers
    var containerGuru = document.getElementById('containerGuru');

    var container = document.createElement('div');
    container.setAttribute('id', _idGuru);
    container.setAttribute('class', 'idContainer')
    container.setAttribute('OnClick', 'SelectGuru()')

    var container1 = document.createElement('div');
    container1.setAttribute('id', 'Container');
    container1.setAttribute('class', 'card p-2');
    container1.setAttribute('style', 'width: 20rem;');

    var container2 = document.createElement('div');
    container2.setAttribute('id', 'Container');
    container2.setAttribute('class', 'd-flex p-3');
    container2.setAttribute('style', 'height: 8rem;');

    var pfpContainer = document.createElement('div');
    pfpContainer.setAttribute('id', 'Container');
    pfpContainer.setAttribute('classs', 'p-2');

    var infoContainer = document.createElement('div');
    infoContainer.setAttribute('id', 'Container');
    infoContainer.setAttribute('class', 'p-2 mt-2');
    // 

     var pfp = document.createElement('img');
     pfp.setAttribute('style', 'clip-path: circle();width: 75px; height: 75px; object-fit: cover;')
     pfp.setAttribute('src', _pfpGuru)

    var namaGuru = document.createElement('p');
    namaGuru.setAttribute('class', 'h5');
    namaGuru.innerHTML = _namaGuru

    var emailGuru = document.createElement('p');
    emailGuru.setAttribute('class', 'h5');
    emailGuru.innerHTML = _emailGuru
    
    var mapelGuru = document.createElement('p');

    containerGuru.appendChild(container);
    container.appendChild(container1);
    container1.appendChild(container2);

    container2.appendChild(pfpContainer);
    container2.appendChild(infoContainer);

    pfpContainer.appendChild(pfp);

    infoContainer.appendChild(namaGuru);
    infoContainer.appendChild(emailGuru);
    infoContainer.appendChild(mapelGuru);
}


function FetchAllDatabase() {
    firebase.auth().onAuthStateChanged(function (user) {
        database.ref('users/' + user.uid + '/Guru' ).orderByChild('idGuru').once('value')
            .then(function (snaphot) {
                snaphot.forEach(function (ChildSnapshot) {
                    var _idGuru = ChildSnapshot.val().idGuru;

                    database.ref('users/' + _idGuru+ '/UserData').once('value')
                    .then(function (snapshot) {
                        var _namaGuru = snapshot.val().username;
                        var _emailGuru = snapshot.val().email;
                        var _pfpGuru = snapshot.val().profilePicture;

                        addItemTolist(_namaGuru, _idGuru, _emailGuru, _pfpGuru);
                    })
                });
            })
    });
}

window.onload(FetchAllDatabase());

function SelectGuru() {
    var div = document.getElementById('containerGuru');

    div.addEventListener('click', function (e) {
        if (e.target) {
            localStorage.setItem('idGuru', e.target.id);
            window.location.href = "Homework.html"
        }
    }, {
        once: true
    })
}