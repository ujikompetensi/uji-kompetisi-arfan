function userData() {
    firebase.auth().onAuthStateChanged(function (user) {
        database.ref('users/' + user.uid + '/UserData').once('value')
            .then(function (snaphot) {
                var userEmail = snaphot.val().email;
                document.getElementById('emailText').innerHTML = userEmail;

                var username = snaphot.val().username;
                document.getElementById('usernmText').innerHTML = username;

                var role = snaphot.val().role;
                document.getElementById('roleText').innerHTML = role;

                if (role == 'Guru') {
                    var idGuruText = document.getElementById('idGuruText');
                    var idGuruContainer  = document.getElementById('x');

                    database.ref('users/' + user.uid).once('value')
                    .then(function (snaphot) {
                        var idGuru = snaphot.val().idGuru;
                        idGuruText.innerHTML = idGuru;
                        idGuruContainer.style.visibility = 'visible'
        
                    })
                }
            })
    });
}

userData()

function openForm() {
    var x = document.getElementById('myForm');
    x.className = "show"
}

function closeForm() {
    var x = document.getElementById('myForm');
    x.className = "hidden"
    setTimeout(function () {
        x.className = ""
    }, 250)
}

function deleteUser() {
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;

    auth.signInWithEmailAndPassword(email, password)
        .then(() => {
            var user = auth.currentUser

            database.ref("users/" + user.uid).remove(),
                database.ref("Task/" + user.uid).remove().then(
                    user.delete().then(() => {
                        window.location.replace("index.html")
                    })
                )
        });
}

function editPrfl() {
    var editUsername = document.getElementById('editUsername').value;


    if (document.querySelector('#editUsername').value.length == 0) {
        console.log('Username unchanged')
    } else {
        firebase.auth().onAuthStateChanged(function (user) {
            database.ref('/users/' + user.uid).child('UserData').update({
                username : editUsername
            })
        });
    }

    firebase.auth().onAuthStateChanged(function (user) {
        const ref = firebase.storage().ref('userProfile');
        const file = document.querySelector("#upldImg").files[0];
        const name = user.uid;
    
        const metadata = {
            contentType: file.type
        };
    
        const task = ref.child(name).put(file, metadata);
    
        // ref.child('DSC_9077.JPG').delete();
    
        task
        .then(snapshot => snapshot.ref.getDownloadURL())
        .then(url => {
            firebase.auth().onAuthStateChanged(function (user) {
                database.ref('/users/' + user.uid).child('UserData').update({
                    profilePicture : url
                })
            }).then(
                window.location.reload()
            )
        })
    });

    // var storageRef = firebase.storage().ref();
    // storageRef.child('1649318265403-tempImage9bLMh4.png').getDownloadURL().then(function (url) {
    //     console.log("bsbdsbsdbd");
    //     alert(url);
    //     document.getElementById('img').src = url;
    //  }).catch(function (error) {});
}

function signOutUser() {
    auth.signOut().then(() => {
        window.location.replace("index.html")
    })
}